package com.bh.cats.spring.catsmysql.controllers;

import com.bh.cats.spring.catsmysql.forms.FindCatForm;
import com.bh.cats.spring.catsmysql.model.Cat;
import com.bh.cats.spring.catsmysql.repositories.CatRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class FindCatController {

    private final CatRepository catRepository;

    public FindCatController(CatRepository catRepository) {
        this.catRepository = catRepository;
    }

    @GetMapping("/find-cat")
    public String getFindForm(Model model) {

        model.addAttribute("findCatForm", new FindCatForm());

        return "find-cat";
    }

    @PostMapping("/find-cat")
    public String findCat(@ModelAttribute FindCatForm findCatForm, Model model) {

        Iterable<Cat> foundCats = catRepository.findCatByName(findCatForm.getName());

        model.addAttribute("foundCats", foundCats);

        return "found-cats";
    }
}
