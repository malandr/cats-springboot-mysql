package com.bh.cats.spring.catsmysql.controllers;

import com.bh.cats.spring.catsmysql.forms.CreateCatForm;
import com.bh.cats.spring.catsmysql.model.Cat;
import com.bh.cats.spring.catsmysql.repositories.CatRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.logging.Logger;

@Controller
public class CreateCatController {

    private final CatRepository catRepository;
    private final Logger logger = Logger.getLogger(CreateCatController.class.getName());

    public CreateCatController(CatRepository catRepository) {
        this.catRepository = catRepository;
    }

    @GetMapping("/create-cat")
    public String viewCreateForm(Model model) {

        model.addAttribute("createCatForm", new CreateCatForm());

        return "create-cat";
    }

    @PostMapping("/create-cat")
    public String createCat(@ModelAttribute CreateCatForm createCatForm, Model model) {

        Cat cat = new Cat();

        cat.setName(createCatForm.getName());
        cat.setAge(createCatForm.getAge());
        cat.setColor(createCatForm.getColor());

        if (createCatForm.getColor().equals("Black")) {
            cat.setImagePath("../../images/colors/black.jpg");
        } else if (createCatForm.getColor().equals("Red")) {
            cat.setImagePath("../../images/colors/red.jpg");
        } else if (createCatForm.getColor().equals("Grey")) {
            cat.setImagePath("../../images/colors/grey.jpg");
        } else if (createCatForm.getColor().equals("Three-Color")) {
            cat.setImagePath("../../images/colors/three-color.jpg");
        } else if (createCatForm.getColor().equals("White")) {
            cat.setImagePath("../../images/colors/white.jpg");
        } else if (createCatForm.getColor().equals("White and Grey")) {
            cat.setImagePath("../../images/colors/white_and_grey.jpg");
        }

        catRepository.save(cat);

        model.addAttribute("cat", cat);

        return "cat-created";
    }
}
