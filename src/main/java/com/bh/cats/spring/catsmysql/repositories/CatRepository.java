package com.bh.cats.spring.catsmysql.repositories;

import com.bh.cats.spring.catsmysql.model.Cat;
import org.springframework.data.repository.CrudRepository;

public interface CatRepository extends CrudRepository<Cat, Long> {

    Iterable<Cat> findCatByName(String name);
    Cat findCatById(long id);

}
