package com.bh.cats.spring.catsmysql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CatsMysqlApplication {

	public static void main(String[] args) {

		SpringApplication.run(CatsMysqlApplication.class, args);
	}

}
