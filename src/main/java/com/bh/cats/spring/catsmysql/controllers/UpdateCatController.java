package com.bh.cats.spring.catsmysql.controllers;

import com.bh.cats.spring.catsmysql.model.Cat;
import com.bh.cats.spring.catsmysql.repositories.CatRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class UpdateCatController {

    private final CatRepository catRepository;

    public UpdateCatController(CatRepository catRepository) {
        this.catRepository = catRepository;
    }

    @GetMapping("/update-cat")
    public String getUpdateForm(@RequestParam long id, Model model) {

        Cat foundCat = catRepository.findCatById(id);

        model.addAttribute("foundCat", foundCat);

        return "update-cat";
    }

    @PostMapping("/update-cat")
    public String updateCat(@ModelAttribute Cat cat) {

        catRepository.save(cat);

        return "redirect:cats";
    }
}
