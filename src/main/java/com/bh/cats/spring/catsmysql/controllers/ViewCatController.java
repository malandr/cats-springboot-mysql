package com.bh.cats.spring.catsmysql.controllers;

import com.bh.cats.spring.catsmysql.model.Cat;
import com.bh.cats.spring.catsmysql.repositories.CatRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Controller
public class ViewCatController {

    private final CatRepository catRepository;

    public ViewCatController(CatRepository catRepository) {
        this.catRepository = catRepository;
    }

    @GetMapping("/view-all-cats")
    public String showAllCats(Model model) {

        Iterable<Cat> cats = catRepository.findAll();

        model.addAttribute("cats", cats);

        return "cats";
    }

    @GetMapping("/view/cat/{id}")
    public String showSpecificCat(@PathVariable long id, Model model) {

        Cat cat = catRepository.findCatById(id);

        model.addAttribute("cat", cat);

        return "cat";

    }

}
